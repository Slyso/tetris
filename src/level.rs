use graphics::{Context, rectangle};
use opengl_graphics::GlGraphics;
use piston::input::{RenderArgs, UpdateArgs};

use crate::falling_tile::FallingTile;
use crate::field::{Field, SIZE as FIELD_SIZE};
use crate::input::InputBehaviour;
use crate::PlayerInput;
use crate::vector2::{Vector2f, Vector2i};
use graphics::types::Color;

pub struct Level {
    pub field: Field,
    pub falling_tile: Option<FallingTile>,
    pub falling_speed: f64,
}

impl Level {
    pub fn update(&mut self, input: &PlayerInput, args: &UpdateArgs) {
        match &mut self.falling_tile {
            None => {
                self.falling_tile = Some(FallingTile::generate(Vector2i { x: FIELD_SIZE.x / 2, y: 0 }));
            }
            Some(falling_tile) => {
                if input.left.active() {
                    falling_tile.try_to_move([-1, 0].into(), &self.field);
                }
                if input.right.active() {
                    falling_tile.try_to_move([1, 0].into(), &self.field);
                }
                if input.rotate_clockwise.active() || input.rotate_counterclockwise.active() {
                    falling_tile.try_to_rotate(input.rotate_clockwise.active(), &self.field);
                }
                if input.quick_drop.active() {
                    falling_tile.quick_drop(&mut self.field);
                }

                falling_tile.timer += args.dt;

                if falling_tile.timer >= self.falling_speed {
                    falling_tile.timer -= self.falling_speed;

                    if !falling_tile.try_to_move([0, 1].into(), &self.field) {
                        falling_tile.solidify(&mut self.field);
                    }
                }

                if falling_tile.is_solid() {
                    self.falling_tile = None;
                }
            }
        }
    }

    pub fn render(&self, args: &RenderArgs, context: Context, gl: &mut GlGraphics) {
        let window_size = Vector2f { x: args.window_size[0], y: args.window_size[1] };

        let border_size = 5.0;
        let ratio = (FIELD_SIZE.x as f64) / (FIELD_SIZE.y as f64);
        let field_size = Vector2f { x: window_size.y * ratio, y: window_size.y };

        let block_size: f64 = (field_size.x - border_size * (FIELD_SIZE.x + 1) as f64) / FIELD_SIZE.x as f64;
        let field_color = [1.0, 0.0, 1.0, 1.0];

        let top_left = Vector2f {
            x: window_size.x / 2.0 - field_size.x / 2.0,
            y: window_size.y / 2.0 - field_size.y / 2.0,
        };

        rectangle(field_color, [top_left.x, top_left.y, field_size.x, field_size.y, ], context.transform, gl);

        let mut render_block = |position: &Vector2i, color: Color| {
            rectangle(color, [
                top_left.x + border_size + position.x as f64 * (block_size + border_size),
                top_left.y + border_size + position.y as f64 * (block_size + border_size),
                block_size, block_size
            ], context.transform, gl);
        };

        for x in 0..FIELD_SIZE.x {
            for y in 0..FIELD_SIZE.y {
                let position = &Vector2i { x, y };

                match self.field.get_block(position) {
                    None => {}
                    Some(block) => {
                        render_block(position, block.get_render_color());
                    }
                }
            }
        }

        match &self.falling_tile {
            None => {}
            Some(falling_tile) => {
                for position in &falling_tile.block_positions() {
                    render_block(position, falling_tile.color());
                }
            }
        }
    }
}
