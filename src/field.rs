use crate::block::Block;
use crate::vector2::Vector2i;

pub const SIZE: Vector2i = Vector2i { x: 10, y: 20 };

pub struct Field {
    blocks: [[Block; SIZE.y as usize]; SIZE.x as usize],
}

impl Field {
    pub fn new() -> Field {
        Field { blocks: [[Block { color: None }; SIZE.y as usize]; SIZE.x as usize] }
    }

    pub fn get_block(&self, position: &Vector2i) -> Option<Block> {
        let x = position.x;
        let y = position.y;

        if x >= SIZE.x || y >= SIZE.y || x < 0 || y < 0 { return None; }
        Some(self.blocks[x as usize][y as usize])
    }

    pub fn set_block(&mut self, position: &Vector2i, block: Block) {
        if self.get_block(position).is_some() {
            self.blocks[position.x as usize][position.y as usize] = block;
        }
    }

    pub fn is_empty(&self, position: &Vector2i) -> bool {
        match self.get_block(position) {
            None => false,
            Some(block) => block.is_empty()
        }
    }

    pub fn handle_filled_rows(&mut self) {
        for y in 0..SIZE.y as usize {
            if self.row_is_filled(y) {
                for x in 0..SIZE.x as usize {
                    self.blocks[x][y].color = None;
                }

                for y in (0..y as usize).rev() {
                    for x in 0..SIZE.x as usize {
                        self.blocks[x][y + 1] = self.blocks[x][y];
                    }
                }
            }
        }
    }

    fn row_is_filled(&self, y: usize) -> bool {
        for x in 0..SIZE.x as usize {
            if self.blocks[x][y].is_empty() {
                return false;
            }
        }
        true
    }
}
