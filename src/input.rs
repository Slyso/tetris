use piston::{ButtonArgs, ButtonState, Key};
use piston::input::Button::Keyboard;
use piston::input::UpdateArgs;

pub trait InputBehaviour {
    fn update(&mut self, args: &UpdateArgs);
    fn on_input(&mut self, args: &ButtonArgs);
    fn active(&self) -> bool;
}

struct Input {
    key: Key,
    active: bool,
}

pub struct OnPress { input: Input, locked: bool }

pub struct Repeat {
    input: Input,
    state: RepeatState,
    threshold: f64,
    repeat_rate: f64,
    counter: f64,
}

enum RepeatState {
    Inactive,
    BeforeThreshold,
    AfterThreshold,
}

impl InputBehaviour for OnPress {
    fn update(&mut self, _: &UpdateArgs) {
        self.input.active = false;
    }

    fn on_input(&mut self, args: &ButtonArgs) {
        if args.button == Keyboard(self.input.key) {
            match args.state {
                ButtonState::Press => {
                    if !self.locked {
                        self.input.active = true;
                        self.locked = true;
                    }
                }
                ButtonState::Release => {
                    self.locked = false;
                }
            }
        }
    }

    fn active(&self) -> bool { self.input.active }
}

impl InputBehaviour for Repeat {
    fn update(&mut self, args: &UpdateArgs) {
        self.input.active = false;

        match self.state {
            RepeatState::Inactive => {}
            _ => { self.counter += args.dt }
        }

        match self.state {
            RepeatState::BeforeThreshold => {
                if self.counter >= self.threshold {
                    self.input.active = true;
                    self.counter -= self.threshold;
                    self.state = RepeatState::AfterThreshold;
                }
            }
            RepeatState::AfterThreshold => {
                if self.counter >= self.repeat_rate {
                    self.input.active = true;
                    self.counter -= self.repeat_rate;
                }
            }
            _ => {}
        }
    }

    fn on_input(&mut self, args: &ButtonArgs) {
        if args.button == Keyboard(self.input.key) {
            match args.state {
                ButtonState::Press => {
                    match self.state {
                        RepeatState::Inactive => {
                            self.input.active = true;
                            self.state = RepeatState::BeforeThreshold;
                        }
                        _ => {}
                    }
                }
                ButtonState::Release => {
                    self.state = RepeatState::Inactive;
                    self.counter = 0.0;
                }
            }
        }
    }

    fn active(&self) -> bool { self.input.active }
}

impl OnPress {
    fn new(key: Key) -> OnPress {
        OnPress { input: Input { key, active: false }, locked: false }
    }
}

impl Repeat {
    fn new(key: Key, threshold: f64, repeat_rate: f64) -> Self {
        Repeat {
            input: Input { key, active: false },
            state: RepeatState::Inactive,
            threshold,
            repeat_rate,
            counter: 0.0,
        }
    }
}

pub struct PlayerInput {
    pub left: Repeat,
    pub right: Repeat,
    pub rotate_clockwise: OnPress,
    pub rotate_counterclockwise: OnPress,
    pub quick_drop: OnPress,
}

impl PlayerInput {
    pub fn new() -> PlayerInput {
        PlayerInput {
            left: Repeat::new(Key::A, 0.3, 0.07),
            right: Repeat::new(Key::D, 0.3, 0.07),
            rotate_clockwise: OnPress::new(Key::K),
            rotate_counterclockwise: OnPress::new(Key::J),
            quick_drop: OnPress::new(Key::S),
        }
    }

    pub fn update(&mut self, args: &UpdateArgs) {
        self.left.update(args);
        self.right.update(args);
        self.rotate_clockwise.update(args);
        self.rotate_counterclockwise.update(args);
        self.quick_drop.update(args);
    }

    pub fn on_input(&mut self, args: &ButtonArgs) {
        self.left.on_input(args);
        self.right.on_input(args);
        self.rotate_clockwise.on_input(args);
        self.rotate_counterclockwise.on_input(args);
        self.quick_drop.on_input(args);
    }
}
