use graphics::types::Color;

use crate::block::Block;
use crate::field::Field;
use crate::tile::Tile;
use crate::vector2::Vector2i;

pub struct FallingTile {
    tile: Tile,
    offset: Vector2i,
    color: Color,
    is_solid: bool,
    pub timer: f64,
}

impl FallingTile {
    pub fn generate(offset: Vector2i) -> FallingTile {
        let color: Color;

        match rand::random::<u8>() % 7 {
            0 => { color = [1.0, 1.0, 0.0, 1.0] } //yellow
            1 => { color = [0.0, 1.0, 0.0, 1.0] } //green
            2 => { color = [0.0, 0.0, 1.0, 1.0] } //blue
            3 => { color = [1.0, 0.0, 1.0, 1.0] } //pink
            4 => { color = [0.0, 1.0, 1.0, 1.0] } //cyan
            5 => { color = [1.0, 0.5, 0.0, 1.0] } //orange
            6 => { color = [1.0, 0.0, 0.0, 1.0] } //red
            _ => { panic!("Case not covered") }
        }

        FallingTile {
            tile: Tile::random_tile(),
            offset,
            color,
            is_solid: false,
            timer: 0.0,
        }
    }

    /// Returns `true` if the position was changed.
    /// The position won't change when a collision would occur
    /// or the falling tile would end up outside of the playable area.
    pub fn try_to_move(&mut self, change: Vector2i, field: &Field) -> bool {
        for position in self.block_positions() {
            if !field.is_empty(&(position + change)) { return false; }
        }

        self.offset = self.offset + change;
        true
    }

    pub fn try_to_rotate(&mut self, clockwise: bool, field: &Field) {
        self.tile.rotate(clockwise);
        if !self.valid_position(field) {
            self.tile.rotate(!clockwise);
        }
    }

    pub fn quick_drop(&mut self, field: &mut Field) {
        while self.try_to_move([0, 1].into(), field) {}
        self.solidify(field);
    }

    pub fn solidify(&mut self, field: &mut Field) {
        if !self.is_solid {
            self.is_solid = true;
            self.copy_into_field(field);
            field.handle_filled_rows();
        }
    }

    pub fn is_solid(&self) -> bool { self.is_solid }

    pub fn color(&self) -> Color { self.color }

    pub fn block_positions(&self) -> Vec<Vector2i> {
        let mut positions: Vec<Vector2i> = vec![];

        for &position in self.tile.get_positions() {
            positions.push(position + self.offset);
        }

        positions
    }

    fn copy_into_field(&self, field: &mut Field) {
        for position in self.block_positions() {
            field.set_block(&position, Block { color: Some(self.color) });
        }
    }

    fn valid_position(&self, field: &Field) -> bool {
        for position in self.block_positions() {
            if !field.is_empty(&position) {
                return false;
            }
        }
        true
    }
}
