use graphics::types::Color;
use graphics::color::WHITE;

#[derive(Copy, Clone)]
pub struct Block {
    pub color: Option<Color>,
}

impl Block {
    pub fn is_empty(&self) -> bool {
        self.color.is_none()
    }

    pub fn get_render_color(&self) -> Color {
        match self.color {
            None => WHITE,
            Some(color) => color
        }
    }
}