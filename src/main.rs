extern crate glutin_window;
extern crate graphics;
extern crate opengl_graphics;
extern crate piston;

use glutin_window::GlutinWindow;
use opengl_graphics::{GlGraphics, OpenGL};
use piston::event_loop::{Events, EventSettings};
use piston::input::{ButtonArgs, ButtonEvent, RenderArgs, RenderEvent, UpdateArgs, UpdateEvent};
use piston::window::WindowSettings;

use crate::field::Field;
use crate::input::PlayerInput;
use crate::level::Level;

mod falling_tile;
mod tile;
mod block;
mod level;
mod input;
mod field;
mod vector2;

const UPDATES_PER_SECOND: u64 = 100;

struct App {
    gl: GlGraphics,
    input: PlayerInput,
    level: Level,
}

impl App {
    fn render(&mut self, args: &RenderArgs) {
        let level = &self.level;

        self.gl.draw(args.viewport(), |context, gl| {
            graphics::clear([1.0, 1.0, 1.0, 1.0], gl);
            level.render(args, context, gl);
        });
    }

    fn update(&mut self, args: &UpdateArgs) {
        self.level.update(&self.input, args);
        self.input.update(args);
    }

    fn on_input(&mut self, args: &ButtonArgs) {
        self.input.on_input(args);
    }
}

fn main() {
    let opengl = OpenGL::V4_0;

    let mut window: GlutinWindow = WindowSettings::new("Tetris", [200, 300])
        .graphics_api(opengl)
        .fullscreen(true)
        .vsync(true)
        .build()
        .unwrap();

    let level = Level {
        field: Field::new(),
        falling_tile: None,
        falling_speed: 0.15,
    };

    let mut app = App {
        gl: GlGraphics::new(opengl),
        input: PlayerInput::new(),
        level,
    };

    let mut events = Events::new(EventSettings {
        max_fps: 100,
        ups: UPDATES_PER_SECOND,
        ups_reset: 5,
        swap_buffers: true,
        bench_mode: false,
        lazy: false,
    });

    while let Some(e) = events.next(&mut window) {
        if let Some(args) = e.render_args() { app.render(&args); }
        if let Some(args) = e.update_args() { app.update(&args); }
        if let Some(args) = e.button_args() { app.on_input(&args); }
    }
}
